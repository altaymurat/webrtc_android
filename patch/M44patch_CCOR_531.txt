diff --git a/webrtc/modules/audio_device/android/java/src/org/webrtc/voiceengine/WebRtcAudioRecord.java b/webrtc/modules/audio_device/android/java/src/org/webrtc/voiceengine/WebRtcAudioRecord.java
index e062ba9..7a96ea6 100644
--- a/webrtc/modules/audio_device/android/java/src/org/webrtc/voiceengine/WebRtcAudioRecord.java
+++ b/webrtc/modules/audio_device/android/java/src/org/webrtc/voiceengine/WebRtcAudioRecord.java
@@ -161,6 +161,10 @@ class  WebRtcAudioRecord {
   private int InitRecording(int sampleRate, int channels) {
     Logd("InitRecording(sampleRate=" + sampleRate + ", channels=" +
         channels + ")");
+    if (audioRecord != null) {
+	Loge("InitRecording() called twice without StopRecording()");
+        return -1;
+    }
     final int bytesPerFrame = channels * (BITS_PER_SAMPLE / 8);
     final int framesPerBuffer = sampleRate / BUFFERS_PER_SECOND;
     byteBuffer = byteBuffer.allocateDirect(bytesPerFrame * framesPerBuffer);
@@ -180,12 +184,6 @@ class  WebRtcAudioRecord {
           AudioFormat.ENCODING_PCM_16BIT);
     Logd("AudioRecord.getMinBufferSize: " + minBufferSize);
 
-    if (aec != null) {
-      aec.release();
-      aec = null;
-    }
-    assertTrue(audioRecord == null);
-
     int bufferSizeInBytes = Math.max(byteBuffer.capacity(), minBufferSize);
     Logd("bufferSizeInBytes: " + bufferSizeInBytes);
     try {
@@ -196,10 +194,14 @@ class  WebRtcAudioRecord {
                                     bufferSizeInBytes);
 
     } catch (IllegalArgumentException e) {
-      Logd(e.getMessage());
+      Loge(e.getMessage());
+      return -1;
+    }
+    if (audioRecord == null ||
+      audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {
+      Loge("Failed to create a new AudioRecord instance");
       return -1;
     }
-    assertTrue(audioRecord.getState() == AudioRecord.STATE_INITIALIZED);
 
     Logd("AudioRecord " +
           "session ID: " + audioRecord.getAudioSessionId() + ", " +
